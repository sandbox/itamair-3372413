# EU OSS Catalogue module builder

Experimental module builder of EU OSS Solutions Catalogue, with federated
sources & repositories

## Requirements

To require with composer this project/module you need to add the following to
the main composer.json file;

    "repositories": [
        {
            "type": "composer",
            "url": "https://packages.drupal.org/8"
        },
        {
            "type": "vcs",
            "url": "https://git.drupalcode.org/sandbox/itamair-3372413.git"
        }
    ],

Check the available versions with composer show (where drupal/foo is the
package name defined in the Sandbox project's composer.json):

    composer show drupal/eu_oss_catalogue --all

Then use:

    composer require drupal/eu_oss_catalogue:1.0.x-dev

to require the module (replace the version string).

**Note:**
The module have internal requirements (requires other php libraries or drupal
modules) listed in it composer.json file

#Installation

Enable the main module with the following drush command:

    drush en eu_oss_catalogue

The module is internally using [drupal features/modules](https://www.drupal.org/project/features)
(inside the "features" folder) that will be automatically enabled generating
the following:

- 'Opensource Solution' (opensource_solution) content type;
- 'OSS Solution Source' (oss_solution_source) taxonomy/vocabulary (whose terms
will be automatically generated along migrations);
- 'Migrations EC OSS Catalogue' (migrations_eu_oss_catalogue) Migrations Group,
to which all the migrations are associated to (depending by);

Enable the submodules with migrations definitions:

- publiccode_yml_repositories:

      drush en publiccode_yml_repositories

this is automatically enabling (through its internal drupal/features) the
migration from to developers.italia APIs (https://api.developers.italia.it/v1/software)

- git_projects_federator

      drush en git_projects_federator

this is automatically enabling (through its internal drupal/features)
migrations from different GitLab and Github repositories
('Git Projects Gitlab Code Europa', 'Git Projects Gitlab Open CoDE',
Git Projects GitHub CTT Spain)

